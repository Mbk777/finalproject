package com.example.user.finalassignement;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.user.finalassignement.data.Item;
import com.example.user.finalassignement.R;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private Resources res;
    private String[] Playlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        res = getResources();
        Playlist = res.getStringArray(R.array.PlayList);
        Item[] songList = new Item[Playlist.length];

        for (int idx = 0; idx < Playlist.length; idx++) {
            songList[idx] = new Item(Playlist[idx],"" ,"");
        }


        final ListView lvinfo = findViewById(R.id.lv_playlist);

        lvinfo.setAdapter(new CustomArrayAdapter(this, 0, songList));
        lvinfo.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}

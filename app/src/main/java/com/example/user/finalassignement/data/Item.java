package com.example.user.finalassignement.data;

public class Item {

    private String Name;
    private String Description;
    private String Country;


    public Item(String name,String description, String country) {
        Name = name;
        Description = description;
        Country = country;

    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getCountry() {
        return Country;
    }
}

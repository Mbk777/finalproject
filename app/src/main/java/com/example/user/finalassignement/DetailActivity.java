package com.example.user.finalassignement;

import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

public class DetailActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener, View.OnClickListener {

    private int position;
    private Resources res;
    private String[] Title;
    private String[] Description;
    private String[] Country;
    private MediaPlayer player ;
    private String[] Music;
    private TextView tvName;
    private TextView tvDesc;
    private TextView tvCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_detail );
        position = getIntent().getIntExtra( "position", 0 );

        res = getResources();
        Title = res.getStringArray(R.array.PlayList);
        Description = res.getStringArray( R.array.Description );
        Country = res.getStringArray( R.array.Country );
        Music = res.getStringArray( R.array.Music );

        tvName = findViewById(R.id.tv_title);
        tvName.setText(Title[position]);

        tvDesc = findViewById(R.id.tv_description);
        tvDesc.setText(Description[position]);

        tvCountry = findViewById( R.id.tv_country );
        tvCountry.setText(Country[position]);

        Button btnPlay = findViewById(R.id.btn_start);
        btnPlay.setOnClickListener(this);
        Button btnStop = findViewById(R.id.btn_stop);
        btnStop.setOnClickListener(this);
        Button btnPreview = findViewById(R.id.btn_preview);
        btnPreview.setOnClickListener(this);
        Button btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);

        player = new MediaPlayer();



    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_start :
                if(player != null && !player.isPlaying()) {
                    Uri trackUri = Uri.parse("android.resource://com.example.user.finalassignement" + R.raw.bensoundbrazilsamba);
                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    try {
                        player.setDataSource(this, trackUri);
                        player.setOnPreparedListener(this);
                        player.prepareAsync();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.btn_stop:
                if(player.isPlaying()){
                    player.stop();
                }
                break;

            case R.id.btn_preview:
                tvName.setText(Title[position-1]);
                tvDesc.setText(Description[position-1]);
                tvCountry.setText(Country[position-1]);
                break;

            case R.id.btn_next:

                tvName.setText(Title[position+1]);


                tvDesc.setText(Description[position+1]);


                tvCountry.setText(Country[position+1]);
                break;

            default:
                Log.d("tag","no match!");
        }
    }
}

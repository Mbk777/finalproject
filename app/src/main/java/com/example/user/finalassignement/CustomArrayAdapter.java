package com.example.user.finalassignement;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.finalassignement.data.Item;

import static android.support.v4.content.ContextCompat.startActivity;


public class CustomArrayAdapter extends ArrayAdapter<Item> implements MediaPlayer.OnPreparedListener{

    private Context ctx;
    private Item[] itemArray;
    private MediaPlayer player;


    public CustomArrayAdapter(@NonNull Context context, int resource, @NonNull Item[] objects) {
        super(context, resource, objects);

        this.ctx = context;
        this.itemArray = objects;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.playlist_view, null);

        TextView name = convertView.findViewById(R.id.tv_song_name);
        name.setText(itemArray[position].getName());


        final ImageButton play = convertView.findViewById(R.id.ib_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getContext(), "play, " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ctx, DetailActivity.class);
                intent.putExtra("position", position);
//                intent.putExtra("description", )
                startActivity(ctx, intent, null);
            }
        });




        return convertView;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }


}
